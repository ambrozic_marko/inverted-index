# Instructions

## Installation
This project requires Python3.7 with packages bs4 and nltk.

## Running the project
To run the project move to the indexer directory and run: python start.py

The index database is already included. To rebuild the index run the following:

```
from indexer.Indexer import run_indexer
run_indexer()
```
