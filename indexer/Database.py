import sqlite3

conn = sqlite3.connect('inverted-index.db')


class Sqlite:

    @staticmethod
    def insert_ignore_word(word):
        cursor = conn.cursor()
        cursor.execute("INSERT OR IGNORE INTO IndexWord (word) VALUES (?);", [word])
        conn.commit()

    @staticmethod
    def insert_ignore_posting(word, document_name, frequency, indexes):
        cursor = conn.cursor()
        cursor.execute("INSERT OR IGNORE INTO Posting (word, documentName, frequency, indexes) VALUES (?, ?, ?, ?);",
                       [word, document_name, frequency, indexes])
        conn.commit()

    @staticmethod
    def select_words_from_posting(text):
        cursor = conn.cursor()
        query = "SELECT Posting.* FROM IndexWord " \
                "LEFT JOIN Posting ON IndexWord.word = Posting.word " \
                "WHERE IndexWord.word IN (%s)" % ','.join('?' for i in text)

        cursor.execute(
            query,
            text
        )
        return cursor.fetchall()

    @staticmethod
    def test():
        cursor = conn.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        print(cursor.fetchall())
