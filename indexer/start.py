from indexer.Indexer import run_query

run_query("gradbeno dovoljenje")
run_query("gradbeno dovoljenje", False)

run_query("informacijske tehnologije")
run_query("informacijske tehnologije", False)

run_query("samostojni podjetnik")
run_query("samostojni podjetnik", False)

run_query("Sistem SPOT")
run_query("Sistem SPOT", False)

run_query("predelovalne dejavnosti")
run_query("predelovalne dejavnosti", False)

run_query("trgovina")
run_query("trgovina", False)

run_query("social services")
run_query("social services", False)

