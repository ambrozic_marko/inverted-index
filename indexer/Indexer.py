from os import listdir
from os.path import isfile, join
import re
import datetime

from bs4 import BeautifulSoup
import nltk

from indexer.Database import Sqlite
from indexer import Stopwords


class FileReader:
    root_dir = "pages"
    page_dirs = []

    def __init__(self, page_dirs):
        self.page_dirs = page_dirs

    def get_all_files(self):
        files = {}
        for page_dir in self.page_dirs:
            path = self.root_dir + "/" + page_dir
            onlyfiles = [self.root_dir + "/" + page_dir + "/" + f
                         for f in listdir(path) if isfile(join(path, f)) and '.html' in f]
            files[page_dir] = onlyfiles
        return files

    @staticmethod
    def open_file(path):
        f = open(path, "r")
        return f.read()

    @staticmethod
    def get_snippet(path, indexes, stop=" ", full_stop=".", limit=3, total_num_of_snippets=5):
        start = False
        end = False
        content = Preprocessor.extract_text(FileReader.open_file(path))
        snippets = []
        for index in indexes:
            if len(snippets) >= total_num_of_snippets:
                break
            counter = int(index)
            stops_counter = 0
            snippet = ""
            # first go backward
            while 0 < counter < len(content) - 1 and stops_counter <= limit:
                counter -= 1
                snippet += content[counter]

                if content[counter] == stop:
                    stops_counter += 1

                if content[counter] == full_stop:
                    stops_counter = limit

            snippet = snippet[::-1]

            if counter <= 0:
                start = True

            # then go forward
            counter = int(index)
            stops_counter = 0
            while 0 < counter < len(content) - 1 and stops_counter < limit:
                snippet += content[counter]
                counter += 1

                if content[counter] == stop:
                    stops_counter += 1

                if content[counter] == full_stop:
                    stops_counter = limit

            if counter >= len(content) - 1:
                end = True

            snippets.append(snippet.strip())

        snippet_string = " ... ".join(snippets)

        if start:
            snippet_string = "... " + snippet_string

        if end:
            snippet_string = snippet_string + " ..."

        return snippet_string

    def __str__(self):
        return "\n".join(self.page_dirs)


class Preprocessor:
    @staticmethod
    def extract_text(html):
        html_object = BeautifulSoup(html, features="html.parser")

        for script in html_object(["script", "style"]):
            script.extract()

        text = html_object.get_text(separator=" ").replace("\n", " ")
        text = re.sub(' +', ' ', text)
        return text

    @staticmethod
    def tokenize(text):
        return nltk.word_tokenize(text)

    @staticmethod
    def remove_stopwords(tokenized_content):
        return [w for w in tokenized_content if not w in Stopwords.stop_words_slovene]

    @staticmethod
    def lowercase(tokenized_content):
        return [w.lower() for w in tokenized_content]


class InvertedIndexer:
    @staticmethod
    def index_page(content, file_name):
        tokens = Preprocessor.lowercase(  # 4. lowercase words
            Preprocessor.remove_stopwords(  # 3. remove stopwords
                Preprocessor.tokenize(  # 2. tokenize text
                    Preprocessor.extract_text(content)  # 1. extract text
                )
            )
        )

        for token in set(tokens):
            Sqlite.insert_ignore_word(token)
            token_frequency = InvertedIndexer.calculate_word_frequency(token, tokens)
            token_positions = InvertedIndexer.calculate_word_positions(token, Preprocessor.extract_text(content))
            Sqlite.insert_ignore_posting(token, file_name, str(token_frequency), str.join(",", token_positions))

    @staticmethod
    def calculate_word_frequency(word, tokens):
        return tokens.count(word)

    @staticmethod
    def calculate_word_positions(word, content):
        return [str(w.start()) for w in re.finditer(re.escape(word), content, flags=re.IGNORECASE)]


class Query:
    @staticmethod
    def search(text):
        tokens = Preprocessor.lowercase(  # 3. lowercase words
            Preprocessor.remove_stopwords(  # 2. remove stopwords
                Preprocessor.tokenize(text)  # 1. tokenize text
            )
        )

        words = Sqlite.select_words_from_posting(tokens)
        merged_results = Query.merge_results_by_file(words)

        results = []
        for document_name, word in merged_results.items():
            frequency = word["frequencies"]
            indexes = word["indexes"]
            snippet = FileReader.get_snippet(document_name, indexes)
            results.append(
                {
                    "frequencies": frequency,
                    "document": document_name.replace("pages/", ""),
                    "snippet": snippet
                }
            )
        return sorted(results, key=lambda k: int(k['frequencies']), reverse=True)

    @staticmethod
    def search_unindexed(text):
        search_tokens = Preprocessor.lowercase(  # 3. lowercase words
            Preprocessor.remove_stopwords(  # 2. remove stopwords
                Preprocessor.tokenize(text)  # 1. tokenize text
            )
        )

        file_reader = FileReader(["e-prostor.gov.si", "e-uprava.gov.si", "evem.gov.si", "podatki.gov.si"])

        words = []
        for file_group, files in file_reader.get_all_files().items():
            for file in files:
                file_contents = FileReader.open_file(file)
                tokens = Preprocessor.lowercase(  # 4. lowercase words
                    Preprocessor.remove_stopwords(  # 3. remove stopwords
                        Preprocessor.tokenize(  # 2. tokenize text
                            Preprocessor.extract_text(file_contents)  # 1. extract text
                        )
                    )
                )

                for token in search_tokens:
                    token_frequency = InvertedIndexer.calculate_word_frequency(token, tokens)
                    token_positions = InvertedIndexer.calculate_word_positions(token,
                                                                               Preprocessor.extract_text(file_contents))
                    if token_frequency > 0:
                        words.append((token, file, token_frequency, ",".join(token_positions)))

        merged_results = Query.merge_results_by_file(words)

        results = []
        for document_name, word in merged_results.items():
            frequency = word["frequencies"]
            indexes = word["indexes"]
            snippet = FileReader.get_snippet(document_name, indexes)
            results.append(
                {
                    "frequencies": frequency,
                    "document": document_name.replace("pages/", ""),
                    "snippet": snippet
                }
            )
        return sorted(results, key=lambda k: int(k['frequencies']), reverse=True)

    @staticmethod
    def merge_results_by_file(words):
        merged_results = {}
        for word in words:
            w, document_name, frequency, indexes = word
            if document_name in merged_results.keys():
                merged_results[document_name] = {
                    "frequencies": str(int(merged_results[document_name]["frequencies"]) + int(frequency)),
                    "document": document_name.replace("pages/", ""),
                    "words": merged_results[document_name]["words"] + [w],
                    "indexes": merged_results[document_name]["indexes"] + indexes.split(",")
                }
            else:
                merged_results[document_name] = {
                    "frequencies": frequency,
                    "document": document_name.replace("pages/", ""),
                    "words": [w],
                    "indexes": indexes.split(",")
                }
        return merged_results


def run_indexer():
    file_reader = FileReader(["e-prostor.gov.si", "e-uprava.gov.si", "evem.gov.si", "podatki.gov.si"])

    for file_group, files in file_reader.get_all_files().items():
        for file in files:
            file_contents = FileReader.open_file(file)
            InvertedIndexer.index_page(file_contents, file)


def run_query(query_text, use_index=True):
    print("Results for query:", "\"" + query_text + "\"", "\n")
    start = datetime.datetime.now()

    if use_index:
        results = Query.search(query_text)
    else:
        results = Query.search_unindexed(query_text)

    end = datetime.datetime.now()
    execution_time = end - start
    print("  Results found in", str(round(execution_time.total_seconds() * 1000)) + "ms.", "\n")

    print("%13s" % "Frequencies", "%60s" % "Document", "%100s" % "Snippet")
    print("%13s" % ("-" * 11), "%60s" % ("-" * 60), "%100s" % ("-" * 100))
    for result in results:
        print("%13s" % result["frequencies"], "%60s" % result["document"], "%100s" % result["snippet"])
